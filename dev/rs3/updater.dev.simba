program UpdaterTest;
{$define smart}
{$i srl-6/srl.simba}

const
  VC_NAME:          string   = 'iOak';
  VC_GAME:          string   = 'rs3';

  VC_PATH:          string   = AppPath + 'Scripts\' + VC_NAME + '\';

  VC_DEPS_RES:  TStringArray = ['map.png', 'progress.png'];
  VC_DEPS_LIB:  TStringArray = ['TColorInfo.simba', 'gfdgfdgfd.simba'];

  VC_THREAD_URL:    string   = 'https://villavu.com/forum/showthread.php?t=111296';

  VC_PROXY_URL:     string   = 'http://static.frement.net/proxy.php?u=';
  VC_ROOT_URL:      string   = VC_PROXY_URL + 'https://bitbucket.org/KeepBotting/simba-scripts/raw/master/';

  VC_RESOURCE_URL:  string   = VC_ROOT_URL + 'res/' + VC_GAME + '/' + VC_NAME + '/';
  VC_LIBRARY_URL:   string   = VC_ROOT_URL + 'lib/' + VC_GAME + '/';

  VC_VERSION_FILE:  string   = VC_ROOT_URL + 'pub/' + VC_GAME + '/' + VC_NAME + '.version.txt';
  VC_SCRIPT_FILE:   string   = VC_ROOT_URL + 'pub/' + VC_GAME + '/' + VC_NAME + '.simba';
  VC_MOTD_FILE:     string   = VC_RESOURCE_URL + 'motd.txt';

  VC_LOCAL_VERSION: extended = 1.3;

var
  vc_RemoteVersion: extended;

(*
procedure vc_Write(s: string);
~~~~~~~~~
  A wrapper to Simba's write() procedure.
*)
procedure vc_Write(s: string);
begin
  write(VC_NAME + ' Updater: ' + s);
end;

(*
procedure vc_WriteLn(s: string);
~~~~~~~~~
  A wrapper to Simba's writeLn() procedure.
*)
procedure vc_WriteLn(s: string);
begin
  writeLn(VC_NAME + ' Updater: ' + s);
end;

(*
procedure vc_WriteMOTD();
~~~~~~~~~
  Retrieves the appropriate MOTD text from the update server, and prints it out.
*)
procedure vc_WriteMOTD();
var
  i: integer;
  s, c: string;
  sArr: TStringArray;
begin
  s := getPage(VC_MOTD_FILE);
  explodeWrap('#', s, sArr);

  writeLn('Welcome to ' + VC_NAME + ', version ' + floatToStr(VC_LOCAL_VERSION) + '.');
  writeLn('');

  if (s <> '') then
  begin
    writeLn('Message Of The Day: ');
    for i := 0 to (length(sArr) - 1) do
    begin
      writeLn(toStr(i + 1) + '. '  + sArr[i]);
    end;
  end;
end;

(*
function vc_Ping(url, name: string): boolean;
~~~~~~~~~
  Establishes a connection to the given URL and attempts to read data.
  Returns 'true' if any data could be read. Used for checking connectivity before
  attempting to retrieve information.
*)
function vc_Ping(url, name: string): boolean;
begin
  if (getPage(url) <> '') then
  exit(true)
  else begin
    writeLn('Updating could not complete, the ' + name + ' was unreachable.');
    writeLn('');
    writeLn('The script will continue running as version ' + floatToStr(VC_LOCAL_VERSION) + '.');
    writeLn('Please view the script thread at ' + VC_THREAD_URL + ' and use the master link to update manually.');
    exit(false);
  end;
end;

(*
function vc_Fetch(sourceFile, destinationFile: string): integer;
~~~~~~~~~
  Reads data from a remote file 'sourceFile' into a local file 'destinationFile'.
  Returns the size of the 'destinationFile' in bytes.
*)
function vc_Fetch(sourceFile, destinationFile: string): integer;
var
  fileHandle: longInt;
begin
  try
    case (fileExists(destinationFile)) of
      true: fileHandle := openFile(destinationFile, false);

      false:
      begin
        closeFile(createFile(destinationFile));
        fileHandle := rewriteFile(destinationFile, false);
        writeFileString(fileHandle, getPage(sourceFile));
      end;
    end;
  finally
    result := fileSize(fileHandle);
    closeFile(fileHandle);
    if (result = 0) then deleteFile(destinationFile);
  end;
end;

(*
function vc_UpdateScript(): boolean;
~~~~~~~~~
  Determines if a newer version of the script is available. If so, downloads
  the new script and terminates.

  Terminates if an update was required, performed, and completed successfully.
  The user is then expected to run the newer version of the script.

  Returns 'true' if no update was required and no further action was taken.

  Returns 'false' if something unexpected happened.
*)
function vc_UpdateScript(): boolean;
var
  newScriptFile: string;
begin
  if ((not vc_Ping('http://static.frement.net', 'proxy server')) or
     (not vc_Ping('http://status.bitbucket.org', 'update server'))) then
    exit(false);

  vc_RemoteVersion := strToFloatDef(getPage(VC_VERSION_FILE), 0);

  if ((vc_RemoteVersion = 0) or (vc_RemoteVersion < VC_LOCAL_VERSION)) then
  begin
    vc_WriteLn('Error: Updating cannot continue, version number sanity check failed.');
    vc_WriteLn('Expected: >=' + floatToStr(VC_LOCAL_VERSION) + '  |  Received: ' + floatToStr(vc_RemoteVersion) + '.');
    writeLn('');
    writeLn('The script will continue running as version ' + floatToStr(VC_LOCAL_VERSION) + '.');
    writeLn('Please view the script thread at ' + VC_THREAD_URL + ' and use the master link to update manually (if needed).');
    writeLn('');
    exit(false);
  end;

  if (VC_LOCAL_VERSION = vc_RemoteVersion) then
  begin
    vc_WriteLn('No update available.');
    result := true;
  end
  else begin
    newScriptFile := ScriptPath + VC_NAME + ' v' + floatToStr(vc_RemoteVersion) + '.simba';

    vc_WriteLn('You have a new update available for ' + VC_NAME + '!');
    vc_WriteLn('Local Version - "' + floatToStr(VC_LOCAL_VERSION) + '"  |  Remote Version - "' + floatToStr(vc_RemoteVersion) + '"');
    vc_WriteLn('Downloading...');

    if (vc_Fetch(VC_SCRIPT_FILE, newScriptFile) > 0) then
    begin
      vc_WriteLn('New script downloaded to ' + newScriptFile + ', please use this one!');
      writeLn('');
      terminateScript();
    end
    else begin
      vc_WriteLn('Error: Could not download updated script!');
      writeLn('');
      writeLn('The script will continue running as version ' + floatToStr(VC_LOCAL_VERSION) + '.');
      writeLn('Please view the script thread at ' + VC_THREAD_URL + ' and use the master link to update manually.');
      writeLn('');
      result := false;
    end;
  end;
  writeLn('');
end;

(*
procedure vc_ResolveDeps(directory: string; files: TStringArray; what: string);
~~~~~~~~~
  Attempts to resolve dependencies using the remote directory 'directory' and
  the list of files 'files'. Debugs what is happening using 'what'.

  Terminates if something unexpected happened.
*)
procedure vc_ResolveDeps(what, directory: string; files: TStringArray);
var
  i: integer;
  sources, destinations: TStringArray;
begin
  if (not directoryExists(VC_PATH)) then
   createDirectory(VC_PATH);

  vc_WriteLn('This script depends on ' + toStr(length(files)) + ' extra ' + what + 's.');
  setLength(sources, length(files));
  setLength(destinations, length(files));

  for i := 0 to (length(files) - 1) do
  begin
    sources[i] := directory + files[i];
    destinations[i] := VC_PATH + files[i];
  end;

    for i := 0 to (length(files) - 1) do
  begin
    vc_Write('Resolving ' + what + ' dependency (' + toStr(i + 1) + '/' + toStr(length(files)) + ')... ');
    case (vc_Fetch(sources[i], destinations[i])) of
      0:
      begin
        writeLn('error! Could not resolve ' + what + ' dependency!');
        writeLn('');
        writeLn('The script depends on this extra ' + what + ' and cannot continue without it.');
        writeLn('Please try running the script again later so that it can make another attempt at resolving ' + what + ' dependencies.');
        writeLn('For help, post on the script thread at ' + VC_THREAD_URL + ' and be sure to include the entirety of this output.');
        writeLn('');
        writeLn('Failed on ' + what + ': ' + sources[i] + ' with expected path: ' + destinations[i]);
        writeLn('');
        terminateScript();
      end;
      else writeLn('done.');
    end;
  end;
  writeLn('');
end;

begin
  vc_UpdateScript();
  vc_ResolveDeps('resource', VC_RESOURCE_URL, VC_DEPS_RES);
  vc_ResolveDeps('library', VC_LIBRARY_URL, VC_DEPS_LIB);
  vc_WriteMOTD();
end.
