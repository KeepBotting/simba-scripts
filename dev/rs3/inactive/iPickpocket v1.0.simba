program iPickpocket;
{$define smart}
{$i srl-6/srl.simba}
{$i sps/lib/sps-rs3.simba}
{$define debug_on}

const //DON'T touch these!
      //But DO use them as a list of valid choices for the "yourVictim" setup option!
  NPC_MAN = 0;
  NPC_FARMER = 1;
  NPC_MASTER_FARMER = 2;

const //DON'T touch these!
      //But DO use them as a list of valid choices for the "foodType" setup option!
  FOOD_LOBSTER = 0;
  FOOD_SWORDFISH = 1;
  FOOD_SHARK = 2;
  FOOD_MONKFISH = 3;

///////////////////////////////////
///////////////////////////////////
///////     Start Setup     ///////Start setting up the script here. Refer to the comments if you don't know what you're doing.
///////////////////////////////////
///////////////////////////////////

const
   (* player info *)
   playerName  = ['lvl3']; //Put your player's name (or nickname, if you set one) here.
   playerFile   = 'default'; //Put your playerfile's name here. Default is 'default'.
   desiredWorld = -1; //Enter your desired world number here. 0 for random, -1 for play button.

   (* globals *)
   yourVictim = NPC_MAN; //Who are we stealing from?
                         //Refer to the thread OR the constants at the top of the script for a list of valid choices.

   foodType = FOOD_LOBSTER; //What are we eating?
                            //Refer to the thread OR the constants at the top of the script for a list of valid choices.

   healWhen = 40; //We'll heal you when your HP drops below this percentage.

   (* options *)
   minMouseSpeed = 50; //MIN speed at which to move the mouse
                       //Default values for these should be just fine. If you must change, lower = slower, higher = faster
   maxMouseSpeed = 75; //MAX speed at which to move the mouse


///////////////////////////////////
///////////////////////////////////
///////     Stop Setup      ///////Don't modify the script ANY FURTHER unless you know what you're doing. You could break stuff!
///////////////////////////////////
///////////////////////////////////

type
  colorInfo = record
  cts:TColorSettings;
  col, tol:integer;
  area:TBox;
end;

var
(* colors *)
  victim:colorInfo;

(* DTMs *)
  foodDtm:integer;

(* walking *)
  map:TSPSArea;
  thePath:TPointArray;

(* prices *)

(* progress report *)

procedure writeDebug(text:string);
begin
  {$ifdef debug_on}
  writeLn('[DEBUG]     : ' + text);
  {$endif}
end;

procedure writeWarn(text:string);
begin
  {$ifdef debug_on}
  writeln('');
  writeLn('[WARNING]   : ' + text);
  writeln('');
  {$endif}
end;

procedure writeError(text:string);
begin
  {$ifdef debug_on}
  writeln('');
  writeLn('[ERROR]     : ' + text);
  writeln('');
  {$endif}
end;

function getPrice(theItem:string):integer;
var
  thePage, thePrice:string;
  t:TTimeMarker;
begin
  t.start();
  thePage := getPage('http://runescape.wikia.com/wiki/Exchange:' + theItem);
  thePrice := between('GEPrice">', '</span>', thePage);
  thePrice := replace(thePrice, ',', '', [rfReplaceAll]);
  result := strToIntDef(thePrice, 0);
  t.pause();
  theItem := replace(theItem, '_', ' ', [rfReplaceAll]);
  writeDebug('Grabbed the price of ' + theItem + ' (' + thePrice + ') in ' + toStr(t.getTime()) + ' ms.');
end;

procedure getFile(path, url, name:string); //Credits to The Mayor
var
  itemName:string;
  itemFile:longint;
begin
  itemName := (AppPath + path);

  if (FileExists(itemName)) then
  begin
    writeDebug('The file: ' + name + ' already exists');
    exit();
  end;

  ShowMessage('You''re missing the ' + name + '.' + chr(13) + chr(13) + 'Downloading it now!');

  CloseFile(CreateFile(itemName));
  itemFile := RewriteFile(itemName, false);
  WriteFileString(itemFile, GetPage(url));
  CloseFile(itemFile);
end;

function TPointArray.centerPoint():TPoint;
var
  tmp:TPointArray;
begin
  if (length(self) < 1) then
   exit;

  tmp := self;
  tmp.sortFromPoint(self.getMiddle());
  result := tmp[0];
end;

procedure TMufasaBitmap.debugTpa2(tpa:TPointArray; color2:integer); //custom debugging, easy color setting
var
  mid:TPoint;
  l:integer;
  b:TBox;
begin
  b := getTpaBounds(tpa);
  mid := middleTpa(tpa);
  l := length(tpa);

  if (l < 1) then
    exit();

  self.drawTpa(tpa, color2);
end;

function mouseRandom(TPoint:TPoint; mouseAction:integer):boolean;
var
  mouseStyle:array [0..2] of integer;
begin
  mouseStyle := [MOUSE_BREAK, MOUSE_ACCURATE, MOUSE_HUMAN];
  mouseSpeed := randomRange(minMouseSpeed, maxMouseSpeed);
  if (mouseAction = MOUSE_MOVE) then
    missMouse(TPoint, true);
  if (mouseAction <> MOUSE_MOVE) then
    mouse(TPoint, mouseAction, mouseStyle[randomRange(0, 2)]);
end;

procedure initFiles();
begin
  if (not (DirectoryExists(AppPath + 'Scripts\iPickpocket'))) then
   CreateDirectory(AppPath + 'Scripts\iPickpocket');

  getFile('Scripts\iPickpocket\proggy.png', 'http://puu.sh/image.png', 'progress report image');

  case (yourVictim) of
    NPC_MAN: getFile('Includes\SPS\img\edgeville.png', 'http://puu.sh/image.png', 'Edgeville SPS map');
  end;
end;

procedure initScript();
var
  i:integer;
begin
  clearDebug();
  addOnTerminate('stop');
  smartEnableDrawing := true;
  smartShowConsole := false;
  smartPlugins := ['opengl32.dll', 'd3d9.dll'];
  writeDebug('Spawning SMART client...');
  setupSrl();
  disableSrlDebug := true;
  players.setup(playerName, playerFile);
  currentPlayer := 0;
  for i := 0 to high(players) do
   begin
     players[i].world := desiredWorld;
     players[i].isActive := true;
     writeDebug('Logging in...');
   end;
  begin
    if (not players[currentPlayer].login()) then
     exit;
    exitTreasure();
    writeDebug('Setup complete - player is logged in.');
  end;
end;

procedure initPlayer();
begin
  writeDebug('Setting camera.');
  minimap.clickCompass();
  minimap.setAngle(MS_ANGLE_HIGH);

  writeDebug('Setting run.');
  if not (minimap.isRunEnabled()) then
   minimap.toggleRun(true);

  writeDebug('Setting gametab.');
  if (not gameTabs.isTabActive(TAB_BACKPACK)) then
   gameTabs.openTab(TAB_BACKPACK);
end;

procedure initColors();
begin
  case (yourVictim) of

    NPC_MAN:
     begin
       with victim do
        begin
          cts.cts := 2;
          cts.modifier.hue := 3.06;
          cts.modifier.saturation := 0.98;
          col := 5393978;
          tol := 10;
          area := mainScreen.getBounds();
       end;
     end;

    NPC_FARMER:
     begin
       with victim do
        begin
          cts.cts := 2;
          cts.modifier.hue := 1.39;
          cts.modifier.saturation := 1.45;
          col := 1713702;
          tol := 4;
          area := mainScreen.getBounds();
       end;
     end;

    NPC_MASTER_FARMER:
     begin
       with victim do
        begin
          cts.cts := 2;
          cts.modifier.hue := 1.39;
          cts.modifier.saturation := 1.45;
          col := 1713702;
          tol := 4;
          area := mainScreen.getBounds();
       end;
     end;

  end;

  writeDebug('Colors have been initalized.');
end;

procedure initDtms(); //swordfish and lobster were stolen from iKaramjaFisher, lol
begin
  case (foodType) of
    FOOD_LOBSTER: foodDtm := dtmFromString('mFQEAAHicnc1BCkBQAITheUJZUArFglzDfZyWEmGllOQc/sXbWD5T32JmM7Gk3JcKJAgQIkJq9wolarS270aasWDFgQs3HpzYMGHEgL5rePOcZHKP+eGTF0GyDTU=');
  FOOD_SWORDFISH: foodDtm := dtmFromString('mggAAAHicY2NgYOACYiEgZmWAAHYgFgViFgYE4AFibiheU7UNSDJhYDkG7IARB4YAAMSGAnQ=');
      FOOD_SHARK: foodDtm := dtmFromString('mlwAAAHicY2dgYAhjYWAIB2IfIPYD4kggTgTiZCC2YmJgMARiayD2AWJbIPaA8kPs9YG6maCYEYnNxCDCgBsw4sFQAABcnQUw');
   FOOD_MONKFISH: foodDtm := dtmFromString('mggAAAHicY2NgYGBkYWBgAmIGIP7KzMAgCKQFgDiJiYEhGoizgTgeSscAcVSAK1AxEwYWYcAOGHFgCAAAsdYEpA==');
  end;

  writeDebug('DTMs have been initalized.');
end;

procedure initPaths(); //these SPS maps were made possible thanks to BMWxi's SPS World Map, many thanks for his hard work
begin
  case (yourVictim) of
    NPC_MAN: map.setup('edgeville', '');
  end;

  spsAnyAngle := true;
  writeDebug('Map has been initalized.');

  case (yourVictim) of
    NPC_MAN: thePath := [Point(216, 160), Point(239, 152), Point(238, 135), Point(255, 123), Point(222, 110)]; //Bank -> house with men
  end;

  writeDebug('Paths have been initalized.');
end;

procedure verify();
begin
  if (not (inRange(yourVictim, 0, 2))) then
   begin
     writeError('*** FATAL ERROR: yourVictim is invalid! Use NPC_YOURNPC or OBJ_YOUROBJECT');
     terminateScript();
   end;

  if (not (inRange(foodType, 0, 3))) then
   begin
     writeError('*** FATAL ERROR: foodType is invalid! Use FOOD_YOURFOOD');
     terminateScript();
   end;
end;

procedure stop(); //hammer time
begin
  writeWarn('*** Terminating script ***');
  if isLoggedIn() then
   writeDebug('Logging out');
  players[currentPlayer].logout();
  writeDebug('Freeing BMPs');
  writeDebug('Freeing DTMs');
  writeWarn('*** Terminated script ***');
end;

function locate():TPoint;
var
  victims_dist, bank_dist, threshold:integer;
  t:TTimeMarker;
begin
 if (not (isLoggedIn())) then
  exit;

  threshold := 30; //limit in TPoints for how far away we can be from a given location before we say we're not there

  t.start();

  result := map.getPlayerPos();
  if result.equals([-1, -1]) then
   exit;

  victims_dist := distance(result, point(128, 410)); //center of tree grove, right under that little house thingy
  bank_dist := distance(result, point(207, 326)); //center of bank

  if victims_dist < threshold then
   players[currentPlayer].location := 'victims' else
  if bank_dist < threshold then
   players[currentPlayer].location := 'bank' else
  players[currentPlayer].location := 'unknown';

  writeDebug('Our location is currently: ' + players[currentPlayer].location + ', took ' + toStr(t.getTime()) + ' ms.');
end;

function walk(destination:string):boolean;
var
  path:TPointArray;
  currPos:TPoint;
begin
 if (not isLoggedIn()) then
  exit;

  minimap.clickCompass();

  currPos := locate();
  if currPos.equals([-1, -1]) then
   begin
     writeError('Failed to locate player, unable to walk anywhere');
     exit;
   end;

  path := thePath.copy();

  if (destination = 'bank') then
   path.invert();

  writeDebug('Going to walk to: ' + destination);

  if (distance(path[high(path)], currPos) < 10) then
   begin
     writeDebug('Already there!');
     exit(true);
   end;

  result := map.walkPath(path);
end;

begin
  initFiles();
  verify();
  initScript();
  initPlayer();
  initColors();
  initDtms();
  initPaths();
end.
