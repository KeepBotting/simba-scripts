program iFight;
{$define smart}
{$i srl-6/srl.simba}
{$define debug_on} //comment this line to disable script debug messages


const //NO TOUCHY
  version = '1.4';
  playerpos = 1;
  mousepos = 2;
  small = 1;
  medium = 2;
  large = 3;
  SKILL_NONE = -1;

(*
Welcome to...
      _ _______       __    __
     (_) ____(_)___ _/ /_  / /_
    / / /_  / / __ `/ __ \/ __/
   / / __/ / / /_/ / / / / /_
  /_/_/   /_/\__, /_/ /_/\__/
            /____/
*)

///////////////////////////////////Start setting up the script here. Refer to the comments if you don't know what you're doing.
///////////////////////////////////
/////       Start Setup     ///////
///////////////////////////////////
///////////////////////////////////Start setting up the script here. Refer to the comments if you don't know what you're doing.

const
   (* player info *)
   playerNames  = ['']; //Put your player's name (or nickname, if you set one) here.
   playerFile   = 'default'; //Put your playerfile's name here. Default is 'default'.
   desiredWorld = 0; //Enter your desired world number here. 0 for random.

   (* globals *)
   npcName = 'Deadly red spider'; //What's the NAME of the NPC you're killing?
   npcText = 'eadly re'; //What's the TEXT of the NPC you're killing (in the right click choose option menu)?
   npcSize = small; //What's the size of your NPC? (read the thread if you don't know)

   searchLimit = 5; //Minimum number of points to check before re-searching the screen for NPCs
   searchFrom = mousepos; //This dictates how the script searches for NPCs.
                          //mousepos = search from the mouse's position, playerpos = search from the player's position

   { \\\ READ THE THREAD OR BE SMITED! /// }
   npcCol  = 3952028; //What's the color of your NPC?
   npcTol  = 78;      //What's the tolerance of your NPC?
   npcHue  = 0.27;    //What's the hue modifier of your NPC?
   npcSat  = 3.22;    //What's the sat modifier of your NPC?
   npcSens = 15;      //Leave this alone unless you're familliar with CTS2 sensitivity.
   { /// READ THE THREAD OR BE SMITED! \\\ }

   xpPerKill = 530.5; //How much XP do you recieve per NPC slain?

   shieldOn = true; //True if your shield is equipped. False if otherwise (note: shield switching current not supported)

   healAtPercent = 60; //We'll heal you if your HP drops below this percent (note: rejuvinate heals 40% of total HP)
   stopAtPercent = 20; //We'll log you out and stop the script if your HP drops below this percent (note: WEAR A RING OF LIFE)

   usingGuthans = false; //True if you've got Guthans equipped. False if not.

   useRejuvenate = true; //True if you're using rejuvenate to heal. False if not.
   rejuvenateKey = 9; //To what key is rejuvinate bound?

   eatFood = false; //True if you're eating food to heal. False if not.
   foodKey = 9; //To what key is your food bound?

   (* options *)
   forceRightClick = false; //True = force the script to right-click and choose-option,
                            //false = random click type (left, right, double)

   noMouseOverText = false; //Set this to true if you're fighting a monster
                            //with a much higher combat level than yourself,
                            //and thus do not have mouseovertext when hovering it.

   takeLoot = true; //True if you want to take loot, false otherwise.
   lootWhenFull = false; //True if you want to continue picking up loot when the backpack is full. False if not.
                         //Use it if you're looting stackable stuff.
   lootWhat = ['Bones', 'hide', '', '', '', '', '']; //Enter the names of what you want to loot here.
                                                      //READ THE THREAD to ensure you've done it correctly, or you'll miss out on loot.

   stopAtSkill = SKILL_NONE; //We'll log you out and stop the script if this skill reaches stopAtLevel.
   stopAtLevel = 99; //See above, the level goes here (1-99)

                //these will need adjusted based on your combat level(s)
                //they're in seconds, NOT milliseconds
   minWait = 2; //MIN wait for fighting, make this HIGHER if it waits too SHORT (clicking on another npc while still fighting)
   maxWait = 6; //MAX wait for fighting, make this LOWER if it waits too LONG (doing nothing for a long time after npc is dead)

   disableDrawing = false; //If set to true, will disable ALL on-screen drawing,
                           //including loading bitmaps into memory,
                           //and initalizing SMART's drawing canvas

   usePaint = true; //True to use on-screen progress report. It will be printed in the debug box regardless.

                       //default values for these should be just fine
                       //if you must change, lower = slower, higher = faster
   minMouseSpeed = 40; //MIN speed at which to move the mouse
   maxMouseSpeed = 50; //MAX speed at which to move the mouse

///////////////////////////////////Don't modify the script ANY FURTHER unless you know what you're doing. You could break stuff!
///////////////////////////////////
/////       Stop Setup      ///////
///////////////////////////////////
///////////////////////////////////Don't modify the script ANY FURTHER unless you know what you're doing. You could break stuff!

type
  colorInfo = record
  cts:TColorSettings;
  col, tol:integer;
  area:TBox;
end;

type
  itemInfo = record
  dtm:integer;
  value:integer;
  mouseOverText:string;
  highAlch:boolean;
  stackable:boolean;
end;

var
(* colors *)
  npc, poison, lootPiles, redCircle, yellowCircle:colorInfo;

(* combat *)
  currentSpot:TBox;

(* looting *)
  northSquare, southSquare, eastSquare, westSquare, fightLocation:TBox;

(* progress report *)
  proggyBmp, fights, loots, startXp:integer;

///////////////////////////////////
///////////////////////////////////
/////        Debugging      ///////
///////////////////////////////////
///////////////////////////////////

procedure writeDebug(text:string);
begin
{$IFDEF debug_on} writeLn('[DEBUG]     : ' + text); {$ENDIF}
end;

procedure writeWarn(text:string);
begin
{$IFDEF debug_on}
writeln('');
writeLn('[WARNING]   : ' + text);
writeln('');
{$ENDIF}
end;

procedure writeError(text:string);
begin
{$IFDEF debug_on}
writeln('');
writeLn('[ERROR]     : ' + text);
writeln('');
{$ENDIF}
end;

///////////////////////////////////
///////////////////////////////////
/////      Initializing     ///////
///////////////////////////////////
///////////////////////////////////

function getPrice(theItem:string):integer;
var
  thePage, thePrice:string;
  t:TTimeMarker;
begin
 t.start;
 thePage := getPage('http://runescape.wikia.com/wiki/Exchange:' + theItem);
 thePrice := between('GEPrice">', '</span>', thePage);
 thePrice := replace(thePrice, ',', '', [rfReplaceAll]);
 result := strToIntDef(thePrice, 0);
 t.pause;
 theItem := replace(theItem, '_', ' ', [rfReplaceAll]);
 writeDebug('Grabbed the price of ' + theItem + ' (' + thePrice + ') in ' + toStr(t.getTime()) + ' ms.');
 t.reset;
end;

procedure initScript();
var i:integer;
begin
   clearDebug();
   addOnTerminate('stop');
   if not (disableDrawing) then
    smartEnableDrawing := true;
   smartShowConsole := false;
   smartPlugins := ['opengl32.dll', 'd3d9.dll'];
   writeDebug('Spawning SMART client...');
   disableSrlDebug := true;
   setupSrl();
   players.setup(playerNames, playerFile);
   currentPlayer := 0;
   for i := 0 to high(players) do
   begin
      players[i].world := desiredWorld;
      players[i].isActive := true;
      writeDebug('Logging in...');
   end;
    begin
     if (not players[currentPlayer].login()) then
      exit;
     exitTreasure();
     writeDebug('Setup complete - player is logged in.');
    end;

  writeDebug('Welcome to...');
  wait(200);
  writeDebug('    _ _______       __    __');
  wait(200);
  writeDebug('   (_) ____(_)___ _/ /_  / /_');
  wait(200);
  writeDebug('  / / /_  / / __ `/ __ \/ __/');
  wait(200);
  writeDebug(' / / __/ / / /_/ / / / / /_');
  wait(200);
  writeDebug('/_/_/   /_/\__, /_/ /_/\__/');
  wait(200);
  writeDebug('          /____/    v' + version + '');
end;

procedure initPlayer();
begin
  writeDebug('Setting camera.');
  minimap.clickCompass();
  mainScreen.setAngle(MS_ANGLE_HIGH);

 writeDebug('Setting run.');
  if not (minimap.isRunEnabled()) then
   minimap.toggleRun(true);

 writeDebug('Setting gametab.');
  if (not gameTabs.isTabActive(TAB_BACKPACK)) then
   gameTabs.openTab(TAB_BACKPACK);

  writeDebug('Player has been initalized.');
end;

procedure initColors();
begin
  with npc do
  begin
    cts.cts := 2;
    cts.modifier.hue := npcHue;
    cts.modifier.saturation := npcSat;
    cts.modifier.sensitivity := npcSens;
    col := npcCol;
    area := mainScreen.getBounds();
    tol := npcTol;
  end;

  with poison do
  begin
    cts.cts := 2;
    cts.modifier.hue := 0.27;
    cts.modifier.saturation := 0.28;
    cts.modifier.sensitivity := 15;
    col := 376194;
    area := actionBar.getBounds();
    tol := 13;
  end;

  with lootPiles do
  begin
    cts.cts := 2;
    cts.modifier.hue := 0.08;
    cts.modifier.saturation := 0.00;
    cts.modifier.sensitivity := 15;
    col := 854783;
    area := mainScreen.getBounds();
    tol := 1;
  end;

  with redCircle do  //TODO let users unput own values for this
  begin
    cts.cts := 2;
    cts.modifier.hue := 0.20;
    cts.modifier.saturation := 0.00;
    cts.modifier.sensitivity := 15;
    col := 198143;
    area := mainScreen.getBounds();
    tol := 1;
  end;

  with yellowCircle do //un-needed?
  begin
    cts.cts := 2;
    cts.modifier.hue := 0.05;
    cts.modifier.saturation := 0.80;
    cts.modifier.sensitivity := 15;
    col := 3786965;
    area := mainScreen.getBounds();
    tol := 2;
  end;
 writeDebug('Colors have been initalized.');
end;

procedure verify();
begin
  if not (shieldOn) then
   begin
     writeError('*** FATAL ERROR: Shield-swtching is not yet supported!');
     terminateScript();
   end;

  if not inRange(npcSize, 1, 3) then
   begin
     writeError('*** FATAL ERROR: npcSize is invalid! Use small, medium, or large');
     terminateScript();
   end;

  if not inRange(searchFrom, 1, 2) then
   begin
     writeError('*** FATAL ERROR: searchFrom is invalid! Use mousepos or playerpos');
     terminateScript();
   end;

  if (useRejuvenate) and (eatFood) then
   begin
     writeError('*** FATAL ERROR: You cannot use rejuvenate and eat food! Pick one or the other');
     terminateScript();
   end;

  if not inRange(stopAtSkill, -1, 25) then //SKILL_NONE..SKILL_DIVINATION
   begin
     writeError('*** FATAL ERROR: stopAtSkill is invalid! Use SKILL_YOURSKILL');
     terminateScript();
   end;

  if not inRange(stopAtLevel, 1, 99) then
   begin
     writeError('*** FATAL ERROR: stopAtLevel is invalid! Use 1-99');
     terminateScript();
   end;
end;

procedure stop(); //hammer time
begin
  writeWarn('*** Terminating script ***');
  if isLoggedIn() then
   writeDebug('Logging out');
  players[currentPlayer].logout();
  writeWarn('*** Terminated script ***');
end;

///////////////////////////////////
///////////////////////////////////
/////      Antipattern      ///////
///////////////////////////////////
///////////////////////////////////

function mouseRandom(TPoint:TPoint; mouseAction:integer):boolean;
var
  mouseStyle:array [0..2] of integer;
begin
  mouseStyle := [MOUSE_BREAK, MOUSE_ACCURATE, MOUSE_HUMAN];
  mouseSpeed := randomRange(minMouseSpeed, maxMouseSpeed);

  if (mouseAction = MOUSE_MOVE) then
    missMouse(TPoint, true);

  if (mouseAction <> MOUSE_MOVE) then
    mouse(TPoint, mouseAction, mouseStyle[randomRange(0, 2)]);
end;

function mouseHuman(TPoint:TPoint; mouseAction:integer):boolean;
begin
  mouseSpeed := randomRange(minMouseSpeed, maxMouseSpeed);

  if (mouseAction = MOUSE_MOVE) then
    missMouse(TPoint, true);

    if (mouseAction = MOUSE_LEFT) then
   begin
     mouse(TPoint, MOUSE_LEFT, MOUSE_BREAK);
   end;

  if (mouseAction = MOUSE_RIGHT) then
   begin
     missMouse(TPoint, true);
     fastClick(MOUSE_RIGHT);
   end;
end;

function randomAngle(turnNorth:boolean):boolean;
begin
case random(4) of
0: minimap.setAngle(MM_DIRECTION_NORTH);
1: minimap.setAngle(MM_DIRECTION_SOUTH);
2:  minimap.setAngle(MM_DIRECTION_EAST);
3:  minimap.setAngle(MM_DIRECTION_WEST);
end;
if turnNorth then
minimap.clickCompass();
mainScreen.setAngle(MS_ANGLE_HIGH);
end;

procedure antiPattern();
begin
  if (not isLoggedIn()) then
   exit;

  case random(100) of
    1: hoverSkill(stopAtSkill);
    2: sleepAndMoveMouse(5000 + random(1000));
    3: smallRandomMouse();
    4: randomRClickItem();
    5: pickUpMouse();
    6: boredHuman();
    7: mouseOffClient(OFF_CLIENT_RANDOM);
    8: hoverRandomSkill();
    9: randomAngle(false);
    10:
  end;
end;

///////////////////////////////////
///////////////////////////////////
///////   Combat detection  ///////
///////////////////////////////////
///////////////////////////////////

function hasTargeted():boolean;
var
  tpa:TPointArray;
  i:integer;
begin
  result := findColorsTolerance(tpa, redCircle.col, redCircle.area, redCircle.tol, redCircle.cts);
  //smartImage.debugTpa(tpa);
end;

function hasBeenTargeted():boolean;
var
  tpa:TPointArray;
begin
  result := findColorsTolerance(tpa, yellowCircle.col, yellowCircle.area, yellowCircle.tol, yellowCircle.cts);
end;

function waitForTarget():boolean;
var
  t:TTimeMarker
begin
  result := false;
  minimap.waitPlayerMoving();
  t.start();
  repeat
    wait(randomRange(250, 500));
  until (hasTargeted()) or (t.getTime() > maxWait);
  t.pause();
  if (hasTargeted()) then
   result := true;
  if (result) then
   writeDebug('Target acquired in ' + toStr(t.getTime()) + ' ms');
end;

///////////////////////////////////
///////////////////////////////////
//////Player Status Management/////
///////////////////////////////////
///////////////////////////////////

function getHpPercent():integer;
begin
  result := actionBar.getHpPercent();
end;

function getAdrenalinePercent():integer;
begin
  result := actionBar.getAdrenalinePercent();
end;

function isAdrenalineFull():boolean;
begin
  result := (actionBar.getAdrenalinePercent() = 100);
  if (result) then
   writeDebug('Player has full adrenaline') else
   writeDebug('Player does not have full adrenaline');
end;

function isAbilityReady(theSlot:integer):boolean;
begin
  if (actionBar.getAbilityCooldown(theSlot) = 100) then
  begin
    writeDebug('Ability in slot ' + toStr(theSlot) + ' is ready');
    exit(true) end else
  begin
    writeDebug('Ability in slot ' + toStr(theSlot) + ' is not ready.');
    exit(false);
  end;
end;

function clickAbility(theSlot:integer):boolean;
begin
  result := actionBar.clickSlot(theSlot);
  writeDebug('Clicking ability in slot ' + toStr(theSlot));
end;

function needToHeal():boolean;
begin
  result := (actionBar.getHpPercent() < healAtPercent);
  if (result) then
   writeWarn('Player is in need of healing') else
   writeDebug('Player is not in need of healing');
end;

function getFreeSlots():TIntegerArray;
var
  theArray:TIntegerArray;
  i:integer;
begin
  for i := 1 to 28 do
  begin
    if not tabBackpack.isItemInSlot(i) then
     begin
       theArray.append(i);
     end;
  end;
  writeDebug('Free backpack slots are: ' + toStr(theArray));
  result := theArray;
end;

function isPoisoned():boolean;
var
  tpa:TPointArray;
begin
  result := findColorsTolerance(tpa, poison.col, actionBar.getBounds(), poison.tol, poison.cts);
  if (result) then
   writeWarn('Player is in need of curing')
  else
   writeDebug('Player is not in need of curing');
end;

function cure():boolean;
begin
  if not (isPoisoned()) then
   exit;

  actionBar.mouseIcon(AB_BAR_HP, MOUSE_RIGHT);
   if chooseOption._select(['oison'], MOUSE_LEFT, true, 1.00, 150) then
    wait(1000);
   if not (isPoisoned) then
    result := true
   else
    result := false;
end;

function waitHeal(maxWait:integer):boolean; //waits up to maxWait for your HP percent to increase
var
  t:TTimeMarker;
  hp1, hp2:integer;
begin
  result := false;
  t.start();
  hp1 := getHpPercent();

  repeat
    wait(randomRange(250, 500));
    hp2 := getHpPercent();
  until (hp2 > hp1) or (t.getTime() > maxWait);

  t.pause();

  if (hp2 > hp1) then
   result := true
  else
   result := false;

  if (result) then
   writeDebug('Player was healed in ' + toStr(t.getTime()) + ' ms');
end;

function heal():boolean;
var
  firstFreeSlot:integer;
begin
  if (not isLoggedIn()) then
   exit(false);

  if not (needToHeal) then
   exit(false);

  if (usingGuthans) then
   begin
     writeError('*** FATAL ERROR: Player is using guthans, yet is in need of healing - assuming guthans has broken');
     terminateScript();
   end;

  if (shieldOn) then
   begin
    clickAbility(rejuvenateKey);
    if waitHeal(5000) then
     result := true;
   end;
end;

function eat():boolean;
var
  hp1, hp2:integer;
begin
  if (not isLoggedIn()) then
   exit;

  if not (needToHeal) then
   exit(false);

  if (usingGuthans) then
   begin
     writeError('*** FATAL ERROR: Player is using guthans, yet is in need of healing - assuming guthans has broken');
     terminateScript();
   end;

  if (not gameTabs.isTabActive(TAB_BACKPACK)) then //idk why, just seems right
   gameTabs.openTab(TAB_BACKPACK);

  if (eatFood) then
   begin
    hp1 := getHpPercent();
    clickAbility(foodKey);
    if waitHeal(5000) then
     result := true;
   end;
end;

procedure managePlayer();
begin
  if (not isLoggedIn()) then
   exit;

  if (needToHeal) then
   begin
     if (useRejuvenate) then
      begin
        if isAdrenalineFull() then
         if isAbilityReady(rejuvenateKey) then
          if heal() then
           writeDebug('Player has been healed via rejuvenation');
      end
      else if (eatFood) then
       begin
         if not eat() then
          begin
            writeDebug('*** FATAL ERROR: Player was unable to eat food!');
            terminateScript();
          end;
       end;
   end;

  if (isPoisoned) then
   if cure() then
    writeDebug('Player has been cured');

  if (getHpPercent() < stopAtPercent) then
  begin
    writeError('*** FATAL ERROR: HP has dropped too low!');
    terminateScript();
  end;
end;

procedure manageSkills();
var
  theLevel:integer;
begin
  if (stopAtSkill = -1) then
   exit;

  if (not gameTabs.isTabActive(TAB_STATS)) then
   gameTabs.openTab(TAB_STATS);

  if (tabStats.getSkillLevel(stopAtSkill) >= stopAtLevel) then
   begin
     writeWarn('The desired skill level has been reached, terminating');
     terminateScript();
   end;
end;

///////////////////////////////////
///////////////////////////////////
///////       Combat        ///////
///////////////////////////////////
///////////////////////////////////

function TPointArray.centerPoint():TPoint;
var
  tpa:TPointArray;
begin
  if (length(self) < 1) then exit;
  tpa := self;
  tpa.sortFromPoint(self.getMiddle);
  result := tpa[0];
end;

function getMousePos2():TPoint; //returns the mouse position as a TPoint
var
  x, y:integer;
begin
  getMousePos(x, y);
  writeDebug('Mouse position: {X = ' + toStr(x) + ', Y = ' + toStr(y) + '}' );
  result := point(x, y);
end;

function findFight(minWait:integer; searchArea:TBox; out fights:T2DPointArray; exitOnFind:boolean):boolean;
var
  t2d:T2DPointArray;
  tpa:TPointArray;
  findTime, i, h:integer;
  t:TTimeMarker;
begin
  if (not isLoggedIn()) then
   exit;

  t2d := [];
  t.start();
  findTime := randomRange(minWait, round(minWait * 1.5)); //add a bit of randomness to search time
  while (t.getTime() < findTime) do
   begin
     if findColorsTolerance(tpa, npc.col, searchArea, npc.tol, npc.cts) then
      if (length(tpa) < searchLimit) then
       begin
         writeWarn('Not enough points found, changing angle');
         result := false;
         randomAngle(false);
       end;
      begin
        if (exitOnFind) then
        exit(true)
      else
       insert(tpa, t2d);
      end;
     wait(randomRange(100, 200)); //why do i have this here?
  end;
  if (length(t2d) > 1) then
   begin
     tpa := t2d.merge();
     tpa.clearEquals();
     t2d := tpa.toAtpa(30, 30);
     if (length(t2d) > 1) then
      begin
        fights := t2d;
        if not (disableDrawing) then
         smartImage.debugAtpa(t2d);
        result := true;
      end else
   begin
     result := false;
   end;
  end else
   exit(false);
  if (result) then
    writeDebug('Found ' + toStr(length(t2d)) + ' npcs');
end;

function clickFight():boolean;
begin
  if not (noMouseOverText) then
   if not isMouseOverText(['Attack']) then
    exit(false);
  if (forceRightClick) then //if we want to force a right-click...
   begin
     writeDebug('Forcing a right-click');
     fastClick(MOUSE_RIGHT); //...force that right-click...
     if chooseOption.select(['Attack ' + npcText]) then
      exit(true) //...and choose the option, then exit.
   end else //if we don't care about our clicktype...
   begin
     case random(3) of //...we'll choose a random one...
     0: begin
          writeDebug('Left-clicking');
          if didClick(true) then
           exit(true);
        end;
     1: begin
          writeDebug('Right-clicking');
          fastClick(MOUSE_RIGHT); //...right-click, choose option end exit...
          if chooseOption.select(['Attack ' + npcText]) then
           exit(true);
        end;
     2: begin
          writeDebug('Double-clicking');
          fastClick(MOUSE_LEFT);
          wait(randomRange(25, 50)); //...or double-click snd exit.
          if didClick(true) then
           exit(true);
        end;
     end;
   end;
end;

{function getfightLocation():TBox; //old, used in build version 0.4 and below

var
  tpa:TPointArray;
begin
  if not (hasTargeted()) then
   exit;

   fightLocation := [];

  if findColorsTolerance(tpa, redCircle.col, northSquare, redCircle.tol, redCircle.cts) then
  begin
    result := northSquare;
    writeDebug('Targeted npc is in the north square');
  end else
  if findColorsTolerance(tpa, redCircle.col, southSquare, redCircle.tol, redCircle.cts) then
  begin
    result := southSquare;
    writeDebug('Targeted npc is in the south square');
  end else
  if findColorsTolerance(tpa, redCircle.col, eastSquare, redCircle.tol, redCircle.cts) then
  begin
    result := eastSquare;
    writeDebug('Targeted npc is in the east square');
  end else
  if findColorsTolerance(tpa, redCircle.col, westSquare, redCircle.tol, redCircle.cts) then
  begin
    result := westSquare;
    writeDebug('Targeted npc is in the west square');
  end else
  begin
    writeWarn('Targeted npc not found');
  end;

  if not (disableDrawing) then
   smartImage.drawBox(result, false, clYellow); //draw it
end;}

function getFightLocation():TBox;
var
  tpa:TPointArray;
begin
  if not (hasTargeted()) then
   exit;

  if findColorsTolerance(tpa, redCircle.col, mainScreen.getBounds(), redCircle.tol, redCircle.cts) then
   result := getTpaBounds(tpa);

  if not (disableDrawing) then
    smartImage.drawBox(result, false, clYellow); //draw it

  writeDebug('Player is in combat with NPC at ' + toStr(result));
end;

procedure waitWhileFighting();
var
 t:TTimeMarker
begin
  t.start();
  repeat
  if (hasTargeted()) or (hasBeenTargeted()) then
   begin
     case random(2) of
      0: wait(randomRange(800, 1200));
      1: sleepAndMoveMouse(randomRange(800, 1200));
     end;
     fightLocation := getFightLocation(); //refresh the fight location continually while in combat
   end;
  until (not hasTargeted()) or (t.getTime() > (maxWait * 1000)); //timeout failsafe
  writeDebug('Player is out of combat');
end;

function fight():boolean;
var
  tpa:TPointArray;
  t2d:T2DPointArray;
  i, h:integer;
begin
  if (not isLoggedIn()) then
   exit;

  currentSpot := [0, 0];
  if findFight(300, mainScreen.getBounds(), t2d, false) then
  begin
    h := high(t2d);
    for i := 0 to h do
     insert(t2d[i].centerPoint, tpa);
    case (searchFrom) of
    1: tpa.sortFromPoint(mainScreen.playerPoint);
    2: tpa.sortFromPoint(getMousePos2());
    end;
     if (length(tpa) > searchLimit) then //fixes "invalid floating point operation" runtime errors
     for i := 0 to searchLimit do
    begin //npcs move quite frequently, so why bother looping through every point?
      mouseHuman(tpa[i].randRange(-3, 3), MOUSE_MOVE);
      if not (noMouseOverText) then //removing the mouseovertext check, again
       if isMouseOverText(['Attack']) then
       break; //don't check the other point(s) if we've already found something, which conserves time and adrenaline
      end;
       if clickFight() then
        begin
          if minimap.isFlagPresent(randomRange(1500, 3000)) then
           minimap.waitPlayerMoving();
          smartImage.clearArea(mainScreen.getBounds());
          if waitForTarget() then
           fightLocation := getFightLocation();
          if (hasTargeted()) then
           begin //pretty sure this fixes the looking-for-loot-when-we-haven't-actually-killed-anything bug
             inc(fights);
             result := true
           end else
            result := false;
          waitWhileFighting();
        end else
         fight();
  end;
end;

///////////////////////////////////
///////////////////////////////////
//////        Looting       ///////
///////////////////////////////////
///////////////////////////////////

function findLoot(minWait:integer; searchArea:TBox; out loots:T2DPointArray; exitOnFind:boolean):boolean;
var
  t2d:T2DPointArray;
  tpa:TPointArray;
  findTime, i, h:integer;
begin
  if (not isLoggedIn()) then
   exit;

  t2d := [];
  findTime := getSystemTime + randomRange(minWait, round(minWait * 1.5));
  while (getSystemTime < findTime) do
   begin
     if findColorsTolerance(tpa, lootPiles.col, searchArea, lootPiles.tol, lootPiles.cts) then
      begin
        if (exitOnFind) then
        exit(true)
      else
       insert(tpa, t2d);
      end;
     wait(randomRange(100, 200));
  end;
  if (length(t2d) > 1) then
   begin
     tpa := t2d.merge();
     tpa.clearEquals();
     t2d := tpa.toAtpa(25, 25); //there should only be one loot pile in one square (duh) so having it split the boxes any smaller is stupid
     if (length(t2d) > 0) then
      begin
        loots := t2d;
        result := true;
      end else
   begin
     result := false;
   end;
  end;
  if (result) then
   writeDebug('Found a loot pile');
end;

function loot():boolean; //TODO add support for looting multiple items from the same pile (i.e. looting piles under the player)
var                      //TODO add a check so we're not trying to click on loot if we haven't killed anything (possibly reset currentSquare)
  tpa:TPointArray;
  t2d:T2DPointArray;
  i, h:integer;
begin
  if (not isLoggedIn()) then
   exit;

  if not (takeLoot) then
   exit;

  mainScreen.setAngle(MS_ANGLE_HIGH); //move this

   wait(1000 + randomRange(250, 500)); //wait for for npc to perform die animation, remove from screen, and drop loot
                                       //TODO change this to a more dynamic wait
  currentSpot := [0, 0];
  if findLoot(500, fightLocation, t2d, false) then
  begin
    if not (disableDrawing) then
     smartImage.drawBox(fightLocation, false, clLime); //draw it green because yay
    h := high(t2d);
    for i := 0 to h do
     insert(t2d[i].centerPoint, tpa);
    tpa.sortFromPoint(mainScreen.playerPoint);
    for i := 0 to h do
     begin
       if tabBackpack.isFull() then
        if not (lootWhenFull) then //check
        begin
          writeWarn('Backpack is full, not picking up loot');
          exit(false);
        end;
       smartImage.drawClippedText('Loot', tpa[i], 'SmallChars', true, clRed);
     end;
    begin
      mouseRandom(tpa[0].randRange(-3, 3), MOUSE_MOVE);
      if isMouseOverText(['Take', 'Attack', 'Open', 'Pick']) then
       fastClick(MOUSE_RIGHT);
       chooseOption.debugOptions();
       if chooseOption._select(lootWhat, MOUSE_LEFT, true, 1.00, 150) then
        inc(loots);
       minimap.waitPlayerMoving();
       tabBackpack.waitForShift(1000);
       chooseOption.close();
    end;
  end;
end;

///////////////////////////////////
///////////////////////////////////
/////        Mainloop       ///////
///////////////////////////////////
///////////////////////////////////

procedure redraw();
var
 xp:integer;
 secs, xp2:extended;
begin
  xp := (fights * xpPerKill);
  secs := (getTimeRunning() / 1000);
  xp2 := ((xp * 60 * 60) / secs);

  clearDebug();

  disguise('Kills: ' + toStr(fights) + '  |  XP: ' + toStr(xp) + '  |  XP/H: ' + toStr(round(xp2)));

  if not (disableDrawing) then
   begin
     smartImage.clearArea(mainScreen.getBounds);
     smartImage.drawRoundBox(intToBox(chatBox.x1, chatBox.y1, chatBox.x2, chatBox.y2), 50, true, 2178411); //idk
   end;

  writeDebug('Welcome to...');
  writeDebug('    _ _______       __    __');
  writeDebug('   (_) ____(_)___ _/ /_  / /_');
  writeDebug('  / / /_  / / __ `/ __ \/ __/');
  writeDebug(' / / __/ / / /_/ / / / / /_');
  writeDebug('/_/_/   /_/\__, /_/ /_/\__/');
  writeDebug('          /____/    v' + version + '');
  writeDebug('');
  writeDebug('by KeepBotting');
  writeDebug('');
  writeDebug('You are fighting ' + npcName + 's, and have killed ' + toStr(fights) + ' of them.');
  writeDebug('In just ' + timeRunning() + ', you have gained ' + toStr(xp) + ' xp.');
  writeDebug('That means you are gaining ' + toStr(round(xp2)) + ' xp per hour!' );
  if (takeLoot) then
   writeDebug('You have also taken loot from the corpses of ' + toStr(loots) + ' ' + npcName + 's.')
  else writeDebug('You have chosen not to take loot from the slain ' + npcName + 's.');
  writeDebug('');
  writeDebug('~/*/~ Script Debug Output Below ~\*\~');
  writeDebug('');
end;

procedure manage();
begin
  managePlayer();
  manageSkills();
end;

procedure everything();
begin
  if (not isLoggedIn()) then
   players[currentPlayer].login();

  redraw();
  manage();
  if fight() then
   loot();
  antiPattern();
  end;

begin
  initScript();
  initPlayer();
  initColors();
  verify();
  while (players.getActive() > 0) do everything();
end.