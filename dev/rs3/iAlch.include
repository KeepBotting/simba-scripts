(*
This is a helper library for iAlch version 3.0. It mainly deals with records and
getters/setters for alchemical items. This library should be considered a
"core file," and editing it is discouraged unless you know what you're doing.

The file type of this lbrary is "include" and not "simba" to discourage editing,
and also because it looks cooler this way.

I originally envisioned this library to work independently of SRL-6, but as I
started adding more and more routines, the unfeasability of that vision became
apparent.

I have henceforth decided to save myself the headache and simply include the
entirety of SRL-6, rather than try fruitlessly to create an independant library,
or a library which attempts to selectively include bits and pieces of SRL-6.

However, I have managed to avoid overloading or overriding any of SRL-6's
routines in this library. All routines contained herein are solely of type
TAlchemyItem. Of this fact I am quite proud.

Since this library includes SRL-6, the main script does not need to.
*)

{$i srl-6/srl.simba}



{
Types
=============
}



(*
type TAlchemyItem
~~~~~~~~~~~~~~~~~~~~~

A type that stores the properties of an alchemical item.

Explanations for each value in the type are as follows:

* Name - The name of this TAlchemyItem
* Value - The market value of this TAlchemyItem
* highAlchValue - See above, using the high-level alchemy value
* lowAlchValue - See above, using the low-level alchemy value
* profit - Represents this TAlchemyItem's profit-per-cast
* slot - The backpack slot in which this TAlchemyItem resides
* amount - The amount of the TAlchemyItem present in 'slot'
* dtm - The string representation of a TAlchemyItem's DTM.
* __dtm - Internal value. The integer representation of a TAlchemyItem's DTM.
*)
type
  TAlchemyItem = record
  name:string;
  value:integer;
  highAlchValue:integer;
  lowAlchValue:integer;
  profit:integer;
  slot:integer;
  amount:integer;
  dtm:string;
  __dtm:integer;
end;



{
Names
=============
}



(*
TAlchemyItem.getName()
~~~~~~~~~~~~~~~~~~~~~

Returns the normal name of a TAlchemyItem.

Example:

.. code-block:: pascal

    writeLn('The name is ' + alchItem.getName());
*)
function TAlchemyItem.getName():string;
begin
  if (self.name = '') then
   begin
     writeLn('getName(): Name not set. Consider calling setName() first.');
     exit('');
   end;

  result := self.name;
end;

(*
TAlchemyItem.getFormatName()
~~~~~~~~~~~~~~~~~~~~~

Returns the name of a TAlchemyItem as it appears in the URL of its RS Wikia
page.

Example:

.. code-block:: pascal

    writeLn('The formatted name is ' + alchItem.getFormatName());
*)
function TAlchemyItem.getFormatName():string;
begin
  if (self.name = '') then
   begin
     writeLn('getFormatName(): Name not set. Consider calling setName() first.');
     exit('');
   end;

  result := replace(self.name, ' ', '_', [rfReplaceAll]);
end;

(*
TAlchemyItem.setName()
~~~~~~~~~~~~~~~~~~~~~

Sets & returns the normal name of a TAlchemyItem.

Example:

.. code-block:: pascal

    alchItem.setName('Red partyhat');
*)
function TAlchemyItem.setName(s:string):string;
begin
  self.name := s;
  result := self.name;
end;



{
Values
=============
}



(*
TAlchemyItem.getValue()
~~~~~~~~~~~~~~~~~~~~~

Returns the market value of a TAlchemyItem.

Example:

.. code-block:: pascal

    writeLn('The value is ' + toStr(alchItem.getValue()));
*)
function TAlchemyItem.getValue():integer;
begin
  if (self.value = 0) then
   begin
     writeLn('getValue(): Value not set. Consider calling setAllValues() first.');
     exit(0);
   end;

  result := self.value;
end;

(*
TAlchemyItem.getHighAlchValue()
~~~~~~~~~~~~~~~~~~~~~

Returns the high-level alchemy value of a TAlchemyItem.

Example:

.. code-block:: pascal

    writeLn('The high-alch value is ' + toStr(alchItem.getHighAlchValue()));
*)
function TAlchemyItem.getHighAlchValue():integer;
begin
  if (self.highAlchValue = 0) then
   begin
     writeLn('getHighAlchValue(): High-alch value not set. Consider calling setAllValues() first.');
     exit(0);
   end;

  result := self.highAlchValue;
end;

(*
TAlchemyItem.getLowAlchValue()
~~~~~~~~~~~~~~~~~~~~~

Returns the low-level alchemy value of a TAlchemyItem.

Example:

.. code-block:: pascal

    writeLn('The low-alch value is ' + toStr(alchItem.getLowAlchValue()));
*)
function TAlchemyItem.getLowAlchValue():integer;
begin
  if (self.lowAlchValue = 0) then
   begin
     writeLn('getLowAlchValue(): Low-alch value not set. Consider calling setAllValues() first.');
     exit(0);
   end;

  result := self.lowAlchValue;
end;

(*
TAlchemyItem.setValue()
~~~~~~~~~~~~~~~~~~~~~

Sets & returns the market value of a TAlchemyItem based on its name.

This description is relevant to the following routines:
* setValue()
* setHighAlchValue()
* setLowAlchValue()

This function can be called with OR without a parameter.

If called WITHOUT, the function will use the name of the TAlchemyItem to
determine the RS Wikia page upon which to find its market value. If no name is
set, the function will exit and return 0.

If called WITH, the function will ignore the TAlchemyItem's name, and simply use
the page given in the parameter. This is done because calling all of the
value-setters individually will produce three seperate getPage() calls -- an
inefficient waste of time.

Aggregating the value-setters by calling setAllValues() produces the same three
calls, plus one more because setProfit() is called. This is why it is
reccommended to use create() when setting up a TAlchemyItem -- the value-setters
are structured to allow for create()'s ability to determine all required data
whilst only having to call getPage() once.

Now comes the caveat: the script writer should NEVER have the need to manually
override this function's default parameter. If a script writer wishes to take
advantage of this extended functionality, they should simply call the create()
routine, which will take care of it for them.

tl;dr use create() unless you're positive that you have a good reason not to.

Example:

.. code-block:: pascal

    alchItem.setValue();
*)
function TAlchemyItem.setValue(page:string = ''):integer;
var
  price, value:string;
begin
  if ((self.name = '') and (page = '')) then
   begin
     writeLn('setValue(): Name not set. Consider calling setName() first.');
     exit(0);
   end;

  if (page = '') then
   price := getPage('http://runescape.wikia.com/wiki/Exchange:' + self.getFormatName())
  else
   price := page;

  value := between('GEPrice">', '</span>', price);
  value := replace(value, ',', '', [rfReplaceAll]);

  self.value := strToIntDef(value, 0);
  result := self.value;
end;

(*
TAlchemyItem.setHighAlchValue()
~~~~~~~~~~~~~~~~~~~~~

Refer to setValue().

Sets & returns the high-level alchemy value of a TAlchemyItem based on its name.


Example:

.. code-block:: pascal

    alchItem.setHighAlchValue();
*)
function TAlchemyItem.setHighAlchValue(page:string = ''):integer;
var
  price, value:string;
begin
  if ((self.name = '') and (page = '')) then
   begin
     writeLn('setHighAlchValue(): Name not set. Consider calling setName() first.');
     exit(0);
   end;

  if (page = '') then
   price := getPage('http://runescape.wikia.com/wiki/Exchange:' + self.getFormatName())
  else
   price := page;

  value := between('High Alchemy: ', 'Value:', price);
  value := replace(value, ',', '', [rfReplaceAll]);

  self.highAlchValue := strToIntDef(value, 0);
  result := self.highAlchValue;
end;

(*
TAlchemyItem.setLowAlchValue()
~~~~~~~~~~~~~~~~~~~~~

Refer to setValue().

Sets & returns the low-level alchemy value of a TAlchemyItem based on its name.

Example:

.. code-block:: pascal

    alchItem.setLowAlchValue();
*)
function TAlchemyItem.setLowAlchValue(page:string = ''):integer;
var
  price, value:string;
begin
  if ((self.name = '') and (page = '')) then
   begin
     writeLn('setLowAlchValue(): Name not set. Consider calling setName() first.');
     exit(0);
   end;

  if (page = '') then
   price := getPage('http://runescape.wikia.com/wiki/Exchange:' + self.getFormatName())
  else
   price := page;

  value := between('Low Alchemy: ', 'High Alchemy:', price);
  value := replace(value, ',', '', [rfReplaceAll]);

  self.lowAlchValue := strToIntDef(value, 0);
  result := self.lowAlchValue;
end;



{
Profit/Loss
=============

Note:

If any routine dealing with profit/loss is called on a TAlchemyItem which does
not have its name or values set, -1 will be returned instead of 0 -- zero being
the norm for all other functions. In the case of isProfitable(), false will be
returned.

The reasoning behind this behaviour is due to safety: if we can't figure out the
rate of profit/loss, let's assume it's a loss (the actual rate being irrelevant,
-1 was chosen for simplicity's sake).
}


(*
TAlchemyItem.getProfit()
~~~~~~~~~~~~~~~~~~~~~

Returns the net profit (or loss) per cast of casting high-level alchemy on
a TAlchemyItem. Low-level alchemy is not included since it is impossible to
profit from.

Profit is returned in integer form. A positive number indicates a net gain,
while a negative number indicates a net loss.


Example:

.. code-block:: pascal

    writeLn('Profit per cast: ' + toStr(alchItem.getProfit()));
*)

function TAlchemyItem.getProfit():integer;
begin
  if (self.profit = 0) then
   begin
     writeLn('getProfit(): Profit not set. Consider calling setAllValues() first.');
     exit(-1);
   end;

   result := self.profit;
end;

(*
TAlchemyItem.getFormatProfit()
~~~~~~~~~~~~~~~~~~~~~

See above, with one exception: returns the profit's absolute value (i.e. always
returns a positive number).
*)

function TAlchemyItem.getFormatProfit():integer;
begin
  if (self.profit = 0) then
   begin
     writeLn('getFormatProfit(): Profit not set. Consider calling setAllValues() first.');
     exit(-1);
   end;

   //result := strToInt(replace(toStr(self.profit), '-', '', [rfReplaceAll]));
   result := abs(self.profit);
end;

(*
TAlchemyItem.setProfit()
~~~~~~~~~~~~~~~~~~~~~

Calculates, sets & returns the net profit (or loss) per cast of casting
high-level alchemy on a TAlchemyItem. Low-level alchemy is not included since
it is impossible to profit from.

Profit is returned in integer form. A positive number indicates a net gain, while
a negative number indicates a net loss.

Example:

.. code-block:: pascal

    alchItem.setProfit();
*)
function TAlchemyItem.setProfit():integer;
var
  n:TAlchemyItem;
begin
  n.setName('Nature rune');

  if (self.value = 0) or (self.highAlchValue = 0) then
   begin
     writeLn('setProfit(): Values not set. Consider calling setAllValues() first.');
     exit(-1);
   end;

  self.profit := (self.highAlchValue - (self.value + n.setValue()));
  result := self.profit;
end;

(*
TAlchemyItem.isProfitable()
~~~~~~~~~~~~~~~~~~~~~

Returns true if it is possible to profit from casting high-level alchemy on
a TAlchemyItem. Returns false otherwise.

Intended as an alternative to getProfit(), in case a script writer has
no need of an actual figure.

Profit is returned in boolean form. "True" indicates a net gain, while "false"
indicates a net loss.

Example:

.. code-block:: pascal

    if (not alchItem.isProfitable()) then
     writeLn('Losing money!');
*)
function TAlchemyItem.isProfitable():boolean;
begin
  result := false;

  if (self.profit = 0) then
   begin
     writeLn('isProfitable(): Profit not set. Consider calling setAllValues() first.');
     exit(false);
   end;

  if (self.profit >= 1) then
   result := true;
end;



{
Slots
=============
}



(*
TAlchemyItem.getSlot()
~~~~~~~~~~~~~~~~~~~~~

Returns the backpack slot in which the TAlchemyItem resides.

Example:

.. code-block:: pascal

    writeLn('The slot is ' + toStr(alchItem.getSlot()));
*)
function TAlchemyItem.getSlot():integer;
begin
  if (self.slot = 0) then
   begin
     writeLn('getSlot(): Slot not set. Consider calling setSlot() first.');
     exit(0);
   end;

  result := self.slot;
end;

(*
TAlchemyItem.setSlot()
~~~~~~~~~~~~~~~~~~~~~

Sets & returns the backpack slot in which the TAlchemyItem resides.

Example:

.. code-block:: pascal

    alchItem.setSlot(tabBackpack.pointToSlot(getMousePos()));
*)
function TAlchemyItem.setSlot(i:integer):integer;
begin
  self.slot := i;
  result := self.slot;
end;

(*
TAlchemyItem.setSlot()
~~~~~~~~~~~~~~~~~~~~~

Overloaded function.

Sets & returns the backpack slot in which the DTM (from string) of the
TAlchemyItem resides. Will exit after finding the first occurrence.

Example:

.. code-block:: pascal

    alchItem.setSlot('m1gAAAHic42JgYFjPxMCwFojnA . . . ');
*)
function TAlchemyItem.setSlot(s:string):integer; overload;
var
  i:integer;
  tmp:TIntegerArray;
begin
  i := DTMFromString(s);

  tmp := findItem(i, tabBackpack.getSlotBoxes(), true);

  if (length(tmp) = 0) then
   begin
     writeLn('setSlot(): DTM not found, unable to set slot by DTM.');
     exit(0);
   end;

  try
    freeDTM(i);
  except
    writeLn('setSlot(): Exception! Unable to free DTM.');
  end;

  self.slot := tmp[0];
  result := self.slot;
end;



{
Amounts
=============
}



(*
TAlchemyItem.getAmount()
~~~~~~~~~~~~~~~~~~~~~

Returns the amount of items in the slot in which the TAlchemyItem resides.

Example:

.. code-block:: pascal

    writeLn('The amount is ' + toStr(alchItem.getAmount()));
*)
function TAlchemyItem.getAmount(i:integer):integer;
begin
  if (self.slot = 0) then
   begin
     writeLn('getAmount(): Amount not set. Consider calling setAmount() first.');
     exit(0);
   end;

  result := self.amount;
end;

(*
TAlchemyItem.setAmount()
~~~~~~~~~~~~~~~~~~~~~

Sets & returns the amount of items in the slot in which the TAlchemyItem resides.

Searches for a default maximum of 1000 ms. Altering this default is not
reccommended, but is possible via overriding the function's parameter.

Rarely, getItemAmount() will incorrectly return -1, hence the logic of this
function and its default maximum wait of 1000 ms.

Concept by The Mayor.

Example:

.. code-block:: pascal

    alchItem.setAmount();
*)
function TAlchemyItem.setAmount(maxWait:integer = 1000):integer;
var
  t:TTimeMarker;
begin
  if (self.slot = 0) then
   begin
     writeLn('setAmount(): Slot not set. Consider calling setSlot() first.');
   end;

  t.start();

  while (t.getTime() < maxWait) do
   begin
     self.amount := getItemAmount(tabBackpack.getSlotBox(self.slot));

     if (self.amount <> -1) then
      exit(self.amount);
   end;
end;



{
DTMs
=============
}


(*
TAlchemyItem.getDTM()
~~~~~~~~~~~~~~~~~~~~~

Returns the DTM associated with the TAlchemyItem. It is the script writer's
responsiibility to free this DTM if they choose to cache it.

Example:

.. code-block:: pascal

    writeLn(tabBackpack.countDTM(alchItem.getDTM()));
*)
function TAlchemyItem.getDTM():integer;
begin
  if (self.dtm = '') then
   begin
     writeLn('getDTM(): DTM string is undefined. Consider calling setDTM() first.');
     exit();
   end;

  result := self.__dtm;
end;

(*
TAlchemyItem.setDTM()
~~~~~~~~~~~~~~~~~~~~~

Creates (from string) sets, and returns the DTM to associated with the
TAlchemyItem. It is the script writer's responsiibility to free this DTM if they
choose to cache it.

Example:

.. code-block:: pascal

    alchItem.setDTM(); //Used when self.dtm is already defined.
*)
function TAlchemyItem.setDTM():integer;
begin
  if (self.dtm = '') then
   begin
     writeLn('setDTM(): DTM string not set. Consider calling setDTM() first.');
     exit(0);
   end;

  self.__dtm := DTMFromString(self.dtm);
  result := self.__dtm;
end;

(*
TAlchemyItem.setDTM()
~~~~~~~~~~~~~~~~~~~~~

Overloaded function.

Creates (from string) sets, and returns the DTM to associated with the
TAlchemyItem.

Uses the DTM string given in the function's paremeter rather than the one that
may or may not already exist in the TAlchemyItem's record.

Example:

.. code-block:: pascal

    alchItem.setDTM('m1gAAAHic42JgYFjPxMCwFojnA . . . ');
*)
function TAlchemyItem.setDTM(s:string):integer; overload;
begin
  self.dtm := s;

  self.__dtm := DTMFromString(self.dtm);
  result := self.__dtm;
end;

(*
TAlchemyItem.__freeDTM()
~~~~~~~~~~~~~~~~~~~~~

Internal routine.

Frees the DTM to associated with the TAlchemyItem. The script writer will call
alchItem.destroy(); to invoke this routine.

Example:

.. code-block:: pascal

    alchItem.__freeDTM(); //Internal usage
    alchItem.destroy();   //External usage
*)
procedure TAlchemyItem.__freeDTM();
begin
  freeDTM(self.__dtm);
end;



{
Helpers
=============
}



(*
TAlchemyItem.create()
~~~~~~~~~~~~~~~~~~~~~

Simultaneously sets the name plus the following values of a TAlchemyItem:

* Value
* High alch value
* Low alch value
* Profit/loss

The usage of this routine is reccommended as it does two things differently:

* Minimizes the number of lines necessary to properly set up a TAlchemyItem
* Uses a single getPage() call as opposed to three when using setAllValues()

A script writer should call this routine if they wish to set the name and all
values at once, but should call the individual setter routines if they wish to
set the name or any values individually.

Example:

.. code-block:: pascal

    alchItem.create('Red partyhat');
*)
procedure TAlchemyItem.create(s:string);
var
  page:string;
begin
  self.setName(s);

  page := getPage('http://runescape.wikia.com/wiki/Exchange:' + self.getFormatName());

  self.setValue(page);
  self.setHighAlchValue(page);
  self.setLowAlchValue(page);
  self.setProfit();
end;

(*
TAlchemyItem.create()
~~~~~~~~~~~~~~~~~~~~~

Overloaded function.

See above, with one exception: this overloaded function accepts a DTM (from
string) as its second parameter, and will call setDTM() and setSlot() with said
parameter.

Therefore, this function will set the name plus the following values:

* Value
* High alch value
* Low alch value
* Profit/loss
* Slot (by DTM)
* Amount

Example:

.. code-block:: pascal

    alchItem.create('Red partyhat', 'm1gAAAHic42JgYFjPxMCwFojnA . . . ');
*)
procedure TAlchemyItem.create(name, s:string); overload;
var
  page:string;
begin
  self.setName(name);

  page := getPage('http://runescape.wikia.com/wiki/Exchange:' + self.getFormatName());

  self.setValue(page);
  self.setHighAlchValue(page);
  self.setLowAlchValue(page);
  self.setProfit();

  self.setDTM(s);
  self.setSlot(self.dtm);
  self.setAmount(self.slot);
end;


(*
TAlchemyItem.setAllValues()
~~~~~~~~~~~~~~~~~~~~~

Sets the following values of a TAlchemyItem, based on its name:

* Value
* High alch value
* Low alch value
* Profit/loss

Usage of create() is reccommended over usage of setAllValues().

A script writer should call this routine if they wish to set all values at once,
but should call the individual setter routines if they wish to set individual
values.
*)
procedure TAlchemyItem.setAllValues();
begin
  if (self.name = '') then
   begin
     writeLn('setAllValues(): Name not set. Consider calling setName() first.');
     exit();
   end;

  self.setValue();
  self.setHighAlchValue();
  self.setLowAlchValue();
  self.setProfit();
end;

(*
TAlchemyItem.debug()
~~~~~~~~~~~~~~~~~~~~~

Prints the following values of a TAlchemyItem:

* Name
* Value
* High alch value
* Low alch value
* Profit/loss
* Slot
* Amount
* DTM

Used for debugging, so null or un-set values will be still be printed.
*)
procedure TAlchemyItem.debug();
begin
  writeLn('TAlchemyItem.debug()');
  writeLn('');
  writeLn('Name: '            + self.name);
  writeLn('Value: '           + toStr(self.value));
  writeLn('High-alch value: ' + toStr(self.highAlchValue));
  writeLn('Low-alch value: '  + toStr(self.lowAlchValue));
  writeLn('Profit/loss: '     + toStr(self.profit));
  writeLn('Slot: '            + toStr(self.slot));
  writeLn('Amount: '          + toStr(self.amount));
  writeLn('DTM: '             + self.dtm);
  writeLn('');
  writeLn('TAlchemyItem.debug()');
end;

(*
TAlchemyItem.destroy()
~~~~~~~~~~~~~~~~~~~~~

Resets all values of a TAlchemyItem to default, as if it was just create()'d.
*)
procedure TAlchemyItem.destroy();
begin
  self.name          := '';
  self.value         := 0;
  self.highAlchValue := 0;
  self.lowAlchValue  := 0;
  self.profit        := 0;
  self.slot          := 0;
  self.amount        := 0;
  self.dtm           := '';
  self.__freeDTM();
end;